<?php

namespace App\Http\Controllers;

use App\FoxKernel\Classes\FoxController;
use App\FoxKernel\Fox;

/**
 * @name InfoController
 * @description Информационный контроллер, для получения всей периферии
 * @author Grigory Alexandrov
 */
class InfoController extends FoxController
{
    /**
     * Экшн получения всех сидов
     * @return array
     */
    public function getCids()
    {
        return Fox::getCids();
    }
}
