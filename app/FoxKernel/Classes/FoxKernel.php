<?php

namespace App\FoxKernel\Classes;

use App\FoxKernel\Interfaces\FoxKernelInterface;

/**
 * @name FoxKernel
 * @description Ядро "FOX"
 * @author Grigory Alexandrov
 */
class FoxKernel extends InnerFoxKernel implements FoxKernelInterface
{
    /**
     * @var static $kernel
     */
    protected static $kernel;

    /**
     * Закрываем конструктор
     */
    private function __construct() { }

    /**
     * Одиночка
     * @return self
     */
    public static function getInstance()
    {
        if (self::$kernel instanceof FoxKernel) {
            return self::$kernel;
        }
         self::$kernel = new self();
        return self::$kernel;
    }

    /**
     * Запуск ядра
     * @return self
     */
    public function run()
    {
        $kernel = self::$kernel;
        $modules = config("module.modules");
        if ($modules) {
            foreach ($modules as $module) {
                $path = __DIR__ . '/../../Modules/';
                //Получаем информацию о модулях
                if (file_exists($path . $module)) {
                    $kernel->loadFoxModules($module);
                }
            }
        }
        return $kernel;
    }
}
