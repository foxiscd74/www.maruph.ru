<?php

namespace App\FoxKernel\Classes;

use App\FoxKernel\Services\Cids\Classes\CidsHelper;
use App\FoxKernel\Services\Cids\Interfaces\CidFoxKernelInterface;
use App\FoxKernel\Interfaces\FoxModuleInterface;
use App\FoxKernel\Services\Logs\Classes\Logger;
use App\FoxKernel\Services\Logs\Interfaces\FoxLoggerInterface;
use App\FoxKernel\Services\Logs\Interfaces\LoggerInterface;
use App\FoxKernel\Services\Response\Classes\Response;
use App\FoxKernel\Services\Response\Interfaces\ResponseInterface;
use Illuminate\Support\Facades\Facade;

/**
 * @name InnerFoxKernel
 * @description Класс внутренней логики ядра
 * @author Grigory ALexandrov
 */
class InnerFoxKernel implements CidFoxKernelInterface, ResponseInterface, FoxLoggerInterface
{
    /**
     * @var FoxModuleInterface[] Информация о модулях системы
     */
    protected $fox_modules_info = [];

    /**
     * Загрузить модули приложения
     * @param $module
     */
    public function loadFoxModules($module)
    {
        $classname_array = glob(__DIR__ . '/../../Modules/' . $module . "/*.php");
        foreach ($classname_array as $filename) {
            $arrNames = (explode('/', $filename));
            $filename = $arrNames[count($arrNames) - 1];
            $classname = 'App\Modules\\' . $module . '\\' . str_replace('.php', '', $filename);
            $class = new $classname;
            if ($class instanceof FoxModuleInterface) {
                $this->fox_modules_info[$module] = $class;
                foreach ($class->getControllers() as $controller) {
                    /* @var FoxController $controller */
                    $controller::runCids();
                }
            }
        }
    }

    /**
     * Получить сиды приложения
     * @return array
     */
    public function getCids()
    {
        return CidsHelper::getInstance()->getResposeCids();
    }

    /**
     * @inheritDoc
     */
    public function getLogger()
    {
        return Logger::getInstance();
    }

    /**
     * @inheritDoc
     */
    public static function getResponseHandler()
    {
        return Response::getInstance();
    }
}
