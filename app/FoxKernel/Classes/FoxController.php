<?php

namespace App\FoxKernel\Classes;

use App\FoxKernel\Fox;
use App\FoxKernel\Services\Cids\Classes\CidsHelper;
use App\FoxKernel\Services\Cids\Interfaces\CidsHelperInterface;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;

/**
 * @property CidsHelper $cidsHelper
 */
abstract class FoxController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $responseHandler;

    public function __construct()
    {
        $this->responseHandler = Fox::getKernel()->getResponseHandler();
    }

    /**
     * Сформировать сиды
     * @return array
     */
    public static function runCids()
    {
        static::setCids();
    }

    /**
     * Получить помощника формирования Сидов
     * @return CidsHelperInterface
     */
    public static function getCidsHelper()
    {
        return CidsHelper::getInstance();
    }

    /**
     * Добавить сиды
     */
    public static function setCids() { }
}
