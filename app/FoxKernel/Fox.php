<?php

namespace App\FoxKernel;

use App\FoxKernel\Classes\FoxKernel;
use App\FoxKernel\Interfaces\FoxKernelInterface;
use Illuminate\Support\Facades\App;

/**
 * @name Fox
 * @description Основной компонент Фокс ядра
 */
class Fox
{
    /**
     * @inheritDoc
     */
    public static function getCids()
    {
        return self::getKernel()->getCids();
    }

    /**
     * @return FoxKernelInterface
     */
    public static function getKernel()
    {
        return App::make(FoxKernel::class);
    }
}
