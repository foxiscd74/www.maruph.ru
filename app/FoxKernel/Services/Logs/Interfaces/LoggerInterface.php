<?php

namespace App\FoxKernel\Services\Logs\Interfaces;

/**
 * Интерфейс логгера
 */
interface LoggerInterface
{
    /**
     * Лоигровать ошибку
     * @param string $message Сообщение
     * @return mixed
     */
    public function logError($message);

    /**
     * Лоигровать информацию
     * @param string $message Сообщение
     * @return mixed
     */
    public function logInfo($message);
}
