<?php

namespace App\FoxKernel\Services\Logs\Interfaces;

/**
 * Интерфейс получения логгера
 */
interface FoxLoggerInterface
{
    /**
     * Получить логера
     * @return LoggerInterface
     */
    public function getLogger();
}
