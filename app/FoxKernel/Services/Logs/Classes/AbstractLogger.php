<?php

namespace App\FoxKernel\Services\Logs\Classes;

abstract class AbstractLogger
{
    const LOGGER_ERROR_DIR = '/var/www/storage/logs/foxError.log';
    const LOGGER_INFO_DIR = '/var/www/storage/logs/foxInfo.log';
    const COUNT_LINES = 1000;

    protected $infoMessage;
    protected $errorMessage;
    protected static $instance;

    /**
     * Закрываем конструктор
     */
    private function __construct() { }

    /**
     * Одиночка CidsHelper
     * @return Logger
     */
    public static function getInstance()
    {
        if (self::$instance instanceof Logger) {
            return self::$instance;
        }
        return self::$instance = new static();
    }

    private function clear($filePath)
    {
        $lines = file($filePath);
        $count = count($lines);
        $i = 0;
        while ($count > self::COUNT_LINES) {
            unset($lines[$i]);
            $i++;
        }
        return implode('', $lines);
    }

    private function push($string, $filePath)
    {
        $lines = $this->clear($filePath);
        file_put_contents($filePath, $lines . $string . PHP_EOL);
    }

    public function write()
    {
        if (!empty($this->errorMessage)) {
            $string = date('Y-m-d H:i:s') . PHP_EOL;
            $strArr = explode('. ', $this->errorMessage);
            foreach ($strArr as $str) {
                $string .= $str . PHP_EOL;
            }
            $this->push($string, self::LOGGER_ERROR_DIR);
        }
        if (!empty($this->infoMessage)) {
            $string = date('Y-m-d H:i:s') . PHP_EOL;
            $strArr = explode('. ', $this->infoMessage);
            foreach ($strArr as $str) {
                $string .= $str . PHP_EOL;
            }
            $this->push($string, self::LOGGER_INFO_DIR);
        }
    }
}
