<?php

namespace App\FoxKernel\Services\Logs\Classes;

use App\FoxKernel\Services\Logs\Interfaces\LoggerInterface;

/**
 * @name Logger
 * Класс логгера.
 * @author Grigory Alexandrov
 * @package App\FoxKernel\Services\Logs\Classes
 */
class Logger extends AbstractLogger implements LoggerInterface
{
    /**
     * @inheritDoc
     */
    public function logError($message)
    {
        $this->errorMessage = $message;
        $this->write();
    }

    /**
     * @inheritDoc
     */
    public function logInfo($message)
    {
        $this->infoMessage = $message;
        $this->write();
    }
}
