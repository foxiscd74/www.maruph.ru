<?php

namespace App\FoxKernel\Services\Response\Classes;

use App\FoxKernel\Services\Response\Interfaces\ResponseHandlerInterface;

/**
 * Абстракция
 */
abstract class AbstractResponse implements ResponseHandlerInterface
{
    /**
     * @var ResponseHandlerInterface
     */
    protected static $responseHandler;
    /**
     * Успешность выполнения
     * @var bool
     */
    protected $success = true;
    /**
     * Ошибки ответа
     * @var array
     */
    protected $errors = [];
    /**
     * Данные ответа
     * @var array
     */
    protected $data = [];
    /**
     * Статус ответа
     * @var int
     */
    protected $status = 200;

    /**
     * Закрываем конструктор
     */
    private function __construct() { }

    /**
     * @inheritDoc
     */
    public function addErrors($errors, $key = null)
    {
        $this->setValues('errors', $errors, $key);
        return self::$responseHandler;
    }

    /**
     * @inheritDoc
     */
    public function addData($data, $key = null)
    {
        $this->setValues('data', $data, $key);
        return self::$responseHandler;
    }

    /**
     * @inheritDoc
     */
    public function setSuccess($bool)
    {
        $this->success = $bool;
        return self::$responseHandler;
    }

    /**
     * @inheritDoc
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return self::$responseHandler;
    }

    /**
     * @inheritDoc
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @inheritDoc
     */
    public function getResponse()
    {
        $response = [
            'success' => $this->success,
            'errors' => $this->errors,
            'data' => $this->data,
            'status' => $this->status
        ];
        return $response;
    }

    /**
     * Добавить данные
     * @param $method
     * @param $data
     * @param null $key
     */
    private function setValues($method, $data, $key = null)
    {
        if (is_array($data) && empty($key)) {
            foreach ($data as $key => $value) {
                if (is_numeric($key)) {
                    $this->$method[] = $value;
                } else {
                    $this->$method[$key] = $value;
                }
            }
        } else {
            if ($key) {
                $this->$method[$key] = $data;
            } else {
                $this->$method[] = $data;
            }
        }
    }

    /**
     * Одиночка
     * @return ResponseHandlerInterface|static
     */
    public static function getInstance()
    {
        self::$responseHandler = new static();
        return self::$responseHandler;
    }
}
