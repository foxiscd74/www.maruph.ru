<?php

namespace App\FoxKernel\Services\Response\Interfaces;

/**
 * Интерфейс ответа
 */
interface ResponseInterface
{
    /**
     * Получить формирователя ответа
     * @return ResponseHandlerInterface
     */
   public static function getResponseHandler();
}
