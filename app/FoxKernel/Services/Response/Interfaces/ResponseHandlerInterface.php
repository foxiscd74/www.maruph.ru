<?php

namespace App\FoxKernel\Services\Response\Interfaces;

interface ResponseHandlerInterface
{
    /**
     * Получить статус
     * @return mixed
     */
    public function getStatus();

    /**
     * Добавить ошибки
     * @param mixed $errors ошибки
     * @param null $key ключ
     * @return ResponseHandlerInterface
     */
    public function addErrors($errors, $key = null);

    /**
     * Добавить данные в запрос
     * @param $data
     * @param null $key
     * @return ResponseHandlerInterface
     */
    public function addData($data, $key = null);

    /**
     * Установить успешность выполнения
     * @param $bool
     * @return ResponseHandlerInterface
     */
    public function setSuccess($bool);

    /**
     * Установить статус выполнения
     * @param $status
     * @return ResponseHandlerInterface
     */
    public function setStatus($status);

    /**
     * Получить ответ
     * @return array
     */
    public function getResponse();
}
