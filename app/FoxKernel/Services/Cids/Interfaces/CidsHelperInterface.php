<?php

namespace App\FoxKernel\Services\Cids\Interfaces;

use App\FoxKernel\Services\Cids\Classes\CidsHelper;

/**
 * @name CidsHelper
 * @description Класс помощник для формирования табов
 * @author Grigory Alexandrov
 */
interface CidsHelperInterface
{
    /**
     * Установить тип
     * @param string $type тип сида
     * @return CidsHelper
     */
    public function setType(string $type);

    /**
     * Установить группу
     * @param string $group группа сида
     * @return CidsHelper
     */
    public function setGroup(string $group);

    /**
     * Установить название
     * @param string $name название сида
     * @return CidsHelper
     */
    public function setName(string $name);

    /**
     * Установить глубину
     * @param int $weight Глубина
     * @return CidsHelper
     */
    public function setWeight(int $weight);

    /**
     * Установить ссылку
     * @param string $link Ссылка
     * @return CidsHelper
     */
    public function setLink(string $link);

    /**
     * Установить название роута
     */
    public function setRouteName(string $name);

    /**
     * Добавить cid, завершает цепочку присвоения
     */
    public function append();

    /**
     * Получить все сиды для ответа
     * @return array
     */
    public function getResposeCids();

    /**
     * Добавить родителя
     */
    public function setParent(string $parent);
}
