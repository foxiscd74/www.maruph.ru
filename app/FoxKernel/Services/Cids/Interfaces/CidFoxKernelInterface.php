<?php

namespace App\FoxKernel\Services\Cids\Interfaces;

/**
 * @name CidFoxKernelInterface
 * @description Пользовательский интерфейс Cids
 */
interface CidFoxKernelInterface
{
    /**
     * Получить сиды приложения
     * @return array
     */
    public function getCids();
}
