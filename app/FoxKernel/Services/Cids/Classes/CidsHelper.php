<?php

namespace App\FoxKernel\Services\Cids\Classes;

use App\FoxKernel\Services\Cids\Interfaces\CidsHelperInterface;

/**
 * @name CidsHelper
 * @description Класс помощник для формирования табов
 * @author Grigory Alexandrov
 * @property CidsHelper $instance Одиночка
 * @property array $cids массив сидов
 * @property array $groups массив групп
 * @property string $type тип сида
 * @property string $group группа сида
 * @property int $weight глубина размещения
 * @property string $link ссылка
 * @property string $name название
 * @property string $parent родительский компонент
 */
class CidsHelper extends FoxCids implements CidsHelperInterface
{
    /**
     * Тип
     * @var string
     */
    protected $type;
    /**
     * Группа
     * @var string
     */
    protected $group;
    /**
     * Название
     * @var string
     */
    protected $name;
    /**
     * Глубина
     * @var int
     */
    protected $weight;
    /**
     * Роут
     * @var string
     */
    protected $route;
    /**
     * УРЛ
     * @var string
     */
    protected $link;
    /**
     * Родительский сид
     * @var string
     */
    protected $parent;

    /**
     * @inheritDoc
     */
    public function append()
    {
        if (!empty($this->parent) || !empty($this->group)) {
            $this->newGroup();
        } else {
            $this->loadCid();
        }
        $this->deleteCidsAttributes();
    }

    /**
     * @inheritDoc
     */
    public function getResposeCids(): array
    {
        return $this->formatAllCids();
    }

    /**
     * @inheritDoc
     */
    public function setType(string $type): CidsHelper
    {
        $this->type = $type;
        return self::getInstance();
    }

    /**
     * @inheritDoc
     */
    public function setGroup(string $group): CidsHelper
    {
        $this->group = $group;
        return self::getInstance();
    }

    /**
     * @inheritDoc
     */
    public function setName(string $name): CidsHelper
    {
        $this->name = $name;
        return self::getInstance();
    }

    /**
     * @inheritDoc
     */
    public function setRouteName(string $name): CidsHelper
    {
        $this->route = $name;
        return self::getInstance();
    }

    /**
     * @inheritDoc
     */
    public function setWeight(int $weight): CidsHelper
    {
        $this->weight = $weight;
        return self::getInstance();
    }

    /**
     * @inheritDoc
     */
    public function setLink(string $link): CidsHelper
    {
        $this->link = $link;
        return self::getInstance();
    }

    /**
     * @inheritDoc
     */
    public function setParent(string $parent): CidsHelper
    {
        $this->parent = $parent;
        return self::getInstance();
    }
}
