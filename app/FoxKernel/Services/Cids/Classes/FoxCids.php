<?php

namespace App\FoxKernel\Services\Cids\Classes;

/**
 * @name FoxCids
 * @description Абстрация для CidsHelper
 * @author Grigory Alexandrov
 */
abstract class FoxCids
{
    private $formatGroups;

    protected $cids = [];
    protected $groups = [];

    protected static $instance;
    protected static $number = 1;

    /**
     * Закрываем конструктор
     */
    private function __construct() { }

    /**
     * Одиночка CidsHelper
     * @return CidsHelper
     */
    public static function getInstance()
    {
        if (self::$instance instanceof CidsHelper) {
            return self::$instance;
        }
        return self::$instance = new static();
    }

    /**
     * Загрузить сиды
     */
    protected function loadCid()
    {
        $this->cids[$this->type][] = [
            'number' => self::$number++,
            'name' => $this->name,
            'link' => url($this->route),
            'weight' => $this->weight,
            'route' => $this->route,
            'type' => $this->type
        ];
        $this->sortByWeight($this->cids[$this->type]);
    }

    /**
     * Отформатировать группы
     * @return array
     */
    protected function formatGroups()
    {
        $formatGroups = [];
        $result = [];
        foreach ($this->groups as $k => $v) {
            if (empty($v['parent'])) {
                $formatGroups[] = $this->groups[$k];
                unset($this->groups[$k]);
            }
        }
        $this->setChilds($formatGroups);
        foreach ($formatGroups as $value) {
            $result[$value['type']][] = $value;
        }
        return $result;
    }

    /**
     * Привести к общему формату все сиды
     * @return array
     */
    protected function formatAllCids()
    {
        $this->formatGroups = $this->formatGroups();
        foreach ($this->cids as $type => $cid) {
            foreach ($this->formatGroups as $typeCid => $group) {
                if ($type == $typeCid) {
                    $result[$type] = array_merge($cid, $group);
                    $this->sortByWeight($result[$type]);
                }
            }
        }
        foreach ($this->cids as $type => $cid) {
            if (empty($result[$type])) {
                $result[$type] = $cid;
            }
        }
        foreach ($this->formatGroups as $type => $cid) {
            if (empty($result[$type])) {
                $result[$type] = $cid;
            }
        }
        return $result;
    }

    /**
     * Добавить дочерние сиды
     * @param array $data массив для изменения
     * @return mixed
     */
    protected function setChilds(&$data)
    {
        foreach ($data as &$v) {
            foreach ($this->groups as $key => $value) {
                if (!empty($v['group']) && $v['group'] == $value['parent']) {
                    $v['childs'][] = $value;
                    unset($this->groups[$key]);
                    $this->setChilds($v['childs']);
                    $this->sortByWeight($v['childs']);
                }
            }
        }
        return $data;
    }

    /**
     * Отсортировать по весу
     * @param array $items массив для сортировки
     * @return mixed
     */
    protected function sortByWeight(&$items)
    {
        usort($items, function ($a, $b) {
            if ($a['weight'] == $b['weight'])
                return 0;
            if ($a['weight'] < $b['weight'])
                return -1;
            else
                return 1;
        });
        return $items;
    }

    /**
     * Добавить новую группу
     */
    protected function newGroup()
    {
        $parentArray = [];
        $groupArray = [];
        if (!empty($this->parent)) {
            $parentArray['parent'] = $this->parent;
        }
        if (!empty($this->group)) {
            $groupArray['group'] = $this->group;
        }
        $arrayGroupe = [
            'number' => self::$number++,
            'name' => $this->name,
            'link' => url($this->route),
            'weight' => $this->weight,
            'route' => $this->route,
            'type' => $this->type
        ];
        $this->groups[] = array_merge($arrayGroupe, $parentArray, $groupArray);
    }

    /**
     * Удалить все атрибуты сида
     */
    protected function deleteCidsAttributes()
    {
        $this->type = null;
        $this->group = null;
        $this->parent = null;
        $this->name = null;
        $this->link = null;
        $this->weight = null;
        $this->routeName = null;
    }
}
