<?php

namespace App\FoxKernel\Interfaces;

/**
 * @name FoxModuleInterface
 * @description Интерфейс подключаемого модуля
 * @author Grigory Alexandrov
 */
interface FoxModuleInterface
{
    /**
     * @description Получить все контроллеры
     * @return array
     */
    public function getControllers();

    /**
     * @description Получить все модели
     * @return array
     */
    public function getModels();
}
