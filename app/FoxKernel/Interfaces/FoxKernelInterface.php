<?php

namespace App\FoxKernel\Interfaces;

use App\FoxKernel\Services\Logs\Classes\Logger;
use App\FoxKernel\Services\Logs\Interfaces\LoggerInterface;
use App\FoxKernel\Services\Response\Interfaces\ResponseHandlerInterface;

/**
 * @name FoxKernelInterface
 * @description Интерфейс ядра "FOX"
 * @author Grigory Alexandrov
 */
interface FoxKernelInterface
{
    /**
     * Получить сиды приложения
     * @return array
     */
    public function getCids();

    /**
     * Получить логера
     * @return LoggerInterface
     */
    public function getLogger();

    /**
     * Получить формирователь ответа
     * @return ResponseHandlerInterface
     */
    public static function getResponseHandler();
}
