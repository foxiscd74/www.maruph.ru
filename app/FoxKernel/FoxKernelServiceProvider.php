<?php

namespace App\FoxKernel;

use App\FoxKernel\Classes\FoxKernel;
use Illuminate\Support\ServiceProvider;

/** * Сервис провайдер для подключения модулей */
class FoxKernelServiceProvider extends ServiceProvider
{

    public function boot()
    {
       FoxKernel::getInstance()->run();
    }

    public function register()
    {
        $this->app->bind(FoxKernel::class, function () {
            return FoxKernel::getInstance();
        });
    }
}
