<?php

namespace App\FoxKernel\Exceptions;

use Exception;

/**
 * Exception для ядра FOX
 */
class FoxKernelException extends Exception
{
}
