<?php

namespace App\Modules\main_module;

use App\FoxKernel\Interfaces\FoxModuleInterface;
use App\Modules\main_module\Controllers\MainController;
use App\Modules\main_module\Models\Customer;
use App\Modules\main_module\Models\Order;
use App\Modules\main_module\Models\User;

/**
 * @name MainModule
 * @description Главный модуль
 * @author Grigory Alexandrov
 */
class MainModule implements FoxModuleInterface
{
    /**
     * @inheritDoc
     * @return string[]
     */
    public function getControllers()
    {
        return [
            MainController::class,
        ];
    }

    /**
     * @inheritDoc
     * @return string[]
     */
    public function getModels()
    {
        return [
            Customer::class,
            Order::class,
            User::class,
        ];
    }
}
