<?php

namespace App\Modules\main_module\Controllers;

use App\FoxKernel\Classes\FoxController;

/**
 * @name MainController
 * @description Главный контроллер
 * @author Grigory Alexandrov
 */
class MainController extends FoxController
{
    /**
     * @inheritDoc
     */
    public static function setCids()
    {
        static::getCidsHelper()
            ->setType('main_menu')
            ->setName('Главная')
            ->setGroup('main')
            ->setRouteName('main')
            ->setWeight(1)
            ->append();
        static::getCidsHelper()
            ->setType('main_menu')
            ->setName('Портфолио')
            ->setRouteName('portfolio')
            ->setWeight(2)
            ->append();
        static::getCidsHelper()
            ->setType('main_menu')
            ->setName('Контакты')
            ->setRouteName('contacts')
            ->setWeight(3)
            ->append();
    }

    /**
     * @return string
     */
    public function index()
    {
        return 'Hello World';
    }
}
