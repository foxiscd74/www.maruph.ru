<?php

namespace App\Modules\main_module\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Модель Клиентов
 * @property int $id Ид клиента
 * @property int $user_id Ид пользователя
 * @property string $created_at Дата создания
 * @property string $updated_at Дата изменения
 */
class Customer extends Model
{
    use HasFactory;
}
