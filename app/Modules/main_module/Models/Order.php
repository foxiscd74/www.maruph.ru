<?php

namespace App\Modules\main_module\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Модель заказов
 * @property int $id Ид заказа
 * @property int $user_id Ид пользователя
 * @property string $created_at Дата создания
 * @property string $updated_at Дата изменения
 */
class Order extends Model
{
    use HasFactory;
}
