<?php

namespace App\Modules\main_module\Models;

use App\Modules\main_module\Traits\ValidateHelper;
use App\Modules\user_interface\Models\Profile;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Redis;
use Laravel\Passport\HasApiTokens;

/**
 * @name User
 * Пользователь
 * @author Grigory Alexandrov
 * @property int $id ИД пользователя
 * @property string $email Email пользователя
 * @property string $password Захешированный пароль пользователя
 * @property string $phone Телефон пользователя
 * @property string $email_verified_at дата верификации почты
 * @property string $created_at Время создания
 * @property string $updated_at Время изменения
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable, ValidateHelper;

    const SCENARIO_REGISTER = 'registration';
    const SCENARIO_LOGIN = 'login';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'login',
        'phone',
        'auth_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function validateScenario()
    {
        return [
            self::SCENARIO_REGISTER => ['email', 'password', 'login', 'phone'],
            self::SCENARIO_LOGIN => ['password', 'email$-unique:users,email'],
        ];
    }

    protected static function rules()
    {
        return [
            'email' => [
                'required' => 'Необходимо заполнить "Email"',
                'email',
                'unique:users,email' => 'Пользователь с таким "Email" существует'
            ],
            'password' => [
                'required' => 'Необходимо заполнить "Пароль"',
                'string',
                'min:6',
                'max:25'
            ],
            'login' => [
                'required' => 'Необходимо заполнить "Логин"',
                'string',
                'min:3',
                'max:25',
                'unique:users,login' => 'Пользователь с таким "Логин" существует'
            ],
            'phone' => [
                'required' => 'Необходимо заполнить "Телефон"',
                'string',
                'unique:users,phone' => "Пользователь с таким телефоном существует",
                'regex:/[0-9]{11}/' => 'Не верный формат телефона'
            ]
        ];
    }

    /**
     * Получить запись с номером телефона пользователя.
     */
    public function profile()
    {
        return $this->hasOne(Profile::class, 'user_id');
    }

    public function setAuthCode($code)
    {
        Redis::set('auth_code_user_' . $this->id, $code);
    }

    public function getAuthCode()
    {
        return Redis::get('auth_code_user_' . $this->id);
    }
}
