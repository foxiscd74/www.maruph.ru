<?php

namespace App\Modules\main_module\Traits;

use Exception;

/**
 * Трейт для преобразования строк правил и сообщений для валидации
 */
trait ValidateHelper
{
    private static $messages = [];
    private static $rules = [];
    public static $scenario;

    /**
     * Правила валидации
     *  Пример написания:
     *  protected static function rules() {
     *      return [
     *           'email' => [
     *               'required' => 'Необходимо заполнить "Email"',
     *               'email',
     *               'unique:users,email' => 'Пользователь с таким "Email" существует'
     *           ],
     *           'password' => ['required' => 'Необходимо заполнить "Пароль"', 'string', 'min:6', 'max:25'],
     *           'login' => ['required' => 'Необходимо заполнить "Логин"', 'string', 'min:3', 'max:25'],
     *           'phone' => [
     *               'required' => 'Необходимо заполнить "Телефон"',
     *               'string',
     *               'unique:users,phone' => "Пользователь с таким телефоном существует",
     *               'regex:/[0-9]{11}/' => 'Не верный формат телефона'
     *           ]
     *      ];
     *  }
     * @return array
     */
    protected static function rules(): array
    {
        return [];
    }

    /**
     * Получить
     * @return array
     * @throws Exception
     */
    private static function getScenarioAttributes()
    {
        $vars = [];
        if (empty(self::validateScenario())) {
            return [];
        }
        if (!empty((self::validateScenario())[self::$scenario])) {
            $scenarioRules = (self::validateScenario())[self::$scenario];
            foreach ($scenarioRules as $rule) {
                $arrVars = explode('$', $rule);
                $vars['attribute'][] = $arrVars[0];
                unset($arrVars[0]);
                if (!empty($arrVars)) {
                    foreach ($arrVars as $var) {
                        $actionSign = $var[0];
                        switch ($actionSign) {
                            case ('-'):
                                $var = str_replace('-', '', $var);
                                $vars['minus'][] = $var;
                                break;
                            case ('+'):
                                $var = str_replace('+', '', $var);
                                $vars['plus'][] = $var;
                                break;
                        }
                    }
                }
            }
            return $vars;
        }
        throw new Exception('Не указан сценарий');
    }

    /**
     * Получить все правила для валидатора
     * @return array
     * @throws Exception
     */
    public static function getRules()
    {
        $scenarioAttributes = self::getScenarioAttributes() ?:
            null;
        if (!empty(self::$rules)) {
            return self::$rules;
        }
        foreach (self::rules() as $attribute => $rules) {
            if (empty($scenarioAttributes) || in_array($attribute, $scenarioAttributes['attribute'])) {
                foreach ($rules as $rule => $message) {
                    if (
                        !empty($scenarioAttributes['minus']) && !in_array($rule, $scenarioAttributes['minus']) &&
                        !in_array($message, $scenarioAttributes['minus']) || empty($scenarioAttributes['minus'])
                    ) {
                        if (empty(self::$rules[$attribute])) {
                            if (gettype($rule) != 'integer') {
                                self::$rules[$attribute] = $rule;
                                $checkRule = explode(':', $rule);
                                self::$messages[$attribute . '.' . $checkRule[0]] = $message;
                            } else {
                                self::$rules[$attribute] = $message;
                            }
                        } else {
                            if (gettype($rule) != 'integer') {
                                self::$rules[$attribute] .= '|' . $rule;
                                $checkRule = explode(':', $rule);
                                self::$messages[$attribute . '.' . $checkRule[0]] = $message;
                            } else {
                                self::$rules[$attribute] .= '|' . $message;
                            }
                        }
                    }
                }
            }
        }
        return self::$rules;
    }

    /**
     * Получить все сообщения для валидатора
     * @return array
     * @throws Exception
     */
    public static function getMessages()
    {
        if (!empty(self::$messages)) {
            return self::$messages;
        }
        self::getRules();
        return self::$messages;
    }

    /**
     * Сценарии валидации
     * @return array
     */
    public static function validateScenario()
    {
        return [];
    }
}
