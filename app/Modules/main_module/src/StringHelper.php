<?php

namespace App\Modules\main_module\src;

/**
 * @name StringHelper
 * Строковый помощник
 */
class StringHelper
{
    /**
     * Получить случайную строку
     * @param int $length Длина строки
     * @return string
     */
    public static function getRandomString(int $length = 32): string
    {
        for (
            $res = '', $i = 0,
            $str = strlen($a = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789') - 1;
            $i != $length; $x = rand(0, $str), $res .= $a[$x], $i++
        ) ;
        return $res;
    }
}
