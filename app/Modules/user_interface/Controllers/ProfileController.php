<?php

namespace App\Modules\user_interface\Controllers;

use App\FoxKernel\Classes\FoxController;
use App\Modules\main_module\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

/**
 * @name ProfileController
 * @description Контроллер профиля
 * @author Grigory Alexandrov
 */
class ProfileController extends FoxController
{
    /**
     * Получить профиль пользователя
     * @param int $id ИД Пользователя
     * @return array
     */
    public function show($id)
    {
        $user = User::where('id', '=', $id)->first();
        $profile = $user->profile;

        return [
            'user' => $user,
            'profile' => $profile
        ];
    }
}
