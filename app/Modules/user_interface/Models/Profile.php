<?php

namespace App\Modules\user_interface\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Модель заказов
 * @property int $id Ид профиля
 * @property string $first_name Имя пользователя
 * @property string $middle_name Отчество пользователя
 * @property string $last_name Фамилия пользователя
 * @property string $address Адрес пользователя
 * @property string $created_at Дата создания
 * @property string $updated_at Дата изменения
 */
class Profile extends Model
{

    protected $table = 'profiles';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'address',
        'user_id',
    ];
}
