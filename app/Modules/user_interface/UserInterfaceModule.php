<?php

namespace App\Modules\user_interface;

use App\FoxKernel\Interfaces\FoxModuleInterface;
use App\Modules\user_interface\Models\Profile;
use App\Modules\user_interface\Controllers\ProfileController;

/**
 * @name UserInterfaceModule
 * @description Главный модуль
 * @author Grigory Alexandrov
 */
class UserInterfaceModule implements FoxModuleInterface
{
    /**
     * @inheritDoc
     * @return string[]
     */
    public function getControllers()
    {
        return [
            ProfileController::class,
        ];
    }

    /**
     * @inheritDoc
     * @return string[]
     */
    public function getModels()
    {
        return [
            Profile::class,
        ];
    }
}
