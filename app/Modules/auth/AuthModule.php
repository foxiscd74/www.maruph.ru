<?php

namespace App\Modules\auth;

use App\FoxKernel\Interfaces\FoxModuleInterface;
use App\Modules\auth\Controllers\AuthController;
use App\Modules\auth\Controllers\SocialController;
use App\Modules\auth\Controllers\VerificationController;

/**
 * @name AuthModule
 * @description Главный модуль
 * @author Grigory Alexandrov
 */
class AuthModule implements FoxModuleInterface
{
    /**
     * @inheritDoc
     * @return string[]
     */
    public function getControllers()
    {
        return [
            AuthController::class,
            SocialController::class,
            VerificationController::class
        ];
    }

    /**
     * @inheritDoc
     * @return string[]
     */
    public function getModels()
    {
        return [
        ];
    }
}
