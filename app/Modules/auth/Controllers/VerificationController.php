<?php

namespace App\Modules\auth\Controllers;

use App\FoxKernel\Classes\FoxController;
use App\Modules\main_module\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

/**
 * @name AuthController
 * @description Контроллер подтверждения электронной почты
 * @author Grigory Alexandrov
 */
class VerificationController extends FoxController
{

    use VerifiesEmails;

    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api')->except(['verify']);
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    /**
     * Проверка
     * @param Request $request
     * @return Redirector|boolean|string
     * @throws AuthorizationException
     */
    public function verify(Request $request)
    {
        $user = User::where('id', $request->route('id'))->first();

        if (empty($user)) {
            return false;
        }
        if (!hash_equals((string)$request->route('id'), (string)$user->getKey())) {
            throw new AuthorizationException;
        }

        if (!hash_equals((string)$request->route('hash'), sha1((string)$user->getEmailForVerification()))) {
            throw new AuthorizationException;
        }

        if ($user->hasVerifiedEmail()) {
            return $request->wantsJson()
                ?
                new JsonResponse([], 204)
                :
                redirect($this->redirectPath());
        }

        if ($user->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }

        return redirect('/account/' . $user->id);
    }
}
