<?php

namespace App\Modules\auth\Controllers;

use App\FoxKernel\Classes\FoxController;
use App\FoxKernel\Fox;
use App\Modules\main_module\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Exception;
use Illuminate\Http\Response;

/**
 * @name AuthController
 * @description Контроллер авторизации и аутентификации
 * @author Grigory Alexandrov
 */
class AuthController extends FoxController
{

    /**
     * Выйти из учетной записи
     * @return Response
     */
    public function logout()
    {
        $response = $this->responseHandler;
        if (Auth::user()) {
            Auth::user()->tokens->each(function ($token) { $token->delete(); });
            return response($response->getResponse(), $response->getStatus());
        }
        return response(
            $response->setSuccess(false)
                ->addErrors('Ошибка авторизации.')
                ->setStatus(401)
                ->getResponse(),
            $response->getStatus()
        );
    }

    /**
     * Войти в учетную запись
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function login(Request $request)
    {
        $response = $this->responseHandler;
        User::$scenario = User::SCENARIO_LOGIN;
        $validator = Validator::make($request->only('email', 'password'), User::getRules(), User::getMessages());
        if ($validator->fails()) {
            $messages = $validator->messages();
            $response->setSuccess(false)->setStatus(500)->addErrors($messages->all());
        } else {
            if (!Auth::attempt($request->only('email', 'password'))) {
                $response->setSuccess(false)->setStatus(401)->addErrors('Ошибка авторизации');
            } else {
                try {
                    $tokenResult = Auth::user()->createToken('Access token');
                    $tokenResult->token->save();
                    $data['user_id'] = Auth::user()->getAuthIdentifier();
                    $data['token_type'] = 'Bearer';
                    $data['token'] = $tokenResult->accessToken;
                    $data['expiried_at'] = Carbon::parse($tokenResult->token->expires_at)->toDateTimeString();
                    $response->addData($data);
                } catch (Exception $e) {
                    Fox::getKernel()->getLogger()->logError(
                        sprintf(
                            '[login] Ошибка формирования токена. %s. %s',
                            $e->getMessage(),
                            $e->getTraceAsString()
                        )
                    );
                    $response->setSuccess(false)->addErrors('Ошибка формирования токена.');
                }
            }
        }
        return response($response->getResponse(), $response->getStatus());
    }

    /**
     * Регистрация пользователя
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function registration(Request $request)
    {
        $response = $this->responseHandler;
        User::$scenario = User::SCENARIO_REGISTER;
        $validator = Validator::make($request->all(), User::getRules(), User::getMessages());
        if ($validator->fails()) {
            $messages = $validator->messages();
            $response->setStatus(500)->setSuccess(false)->addErrors($messages->all());
        } else {
            $user = new User($request->all());
            $user->password = Hash::make($request->post('password'));
            try {
                $user->save();
                event(new Registered($user));
            } catch (Exception $e) {
                Fox::getKernel()->getLogger()->logError(
                    sprintf(
                        '[registration] Ошибка сохранения. %s. %s',
                        $e->getMessage(),
                        $e->getTraceAsString()
                    )
                );
                $response->setStatus(500)->setSuccess(false)->addErrors('Ошибка сохранения');
            }
        }
        return response($response->getResponse(), $response->getStatus());
    }

    /**
     * Получить авторизованного пользователя
     * @return Response
     */
    public function user()
    {
        $user = Auth::user();
        $profile = $user->profile;

        return response(
            [
                'user' => $user,
                'profile' => $profile
            ]
        );
    }
}
