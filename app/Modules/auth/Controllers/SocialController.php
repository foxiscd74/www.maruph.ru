<?php

namespace App\Modules\auth\Controllers;

use App\FoxKernel\Classes\FoxController;
use App\FoxKernel\Fox;
use App\Modules\main_module\Models\User;
use App\Modules\main_module\src\StringHelper;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redis;
use Laravel\Socialite\Facades\Socialite;
use Exception;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Response;

/**
 * @name SocialController
 * Контроллер авторизаций через социальные сети
 * @author Grigory Alexandrov
 */
class SocialController extends FoxController
{

    /**
     * Авторизация
     * @param Request $request
     * @return Response
     */
    public function login(Request $request)
    {
        $response = $this->responseHandler;
        $code = $request->post('code');
        $id = $request->post('id');
        /* @var $user User */
        $user = User::where('id', $id)->first();
        if (!empty($user) && $user->getAuthCode() == $code) {
            try {
                $tokenResult = $user->createToken('Access token');
                $tokenResult->token->save();
                $data['user_id'] = $user->getAuthIdentifier();
                $data['token_type'] = 'Bearer';
                $data['token'] = $tokenResult->accessToken;
                $data['expiried_at'] = Carbon::parse($tokenResult->token->expires_at)->toDateTimeString();
                $response->addData($data);
            } catch (Exception $e) {
                Fox::getKernel()->getLogger()->logError(
                    sprintf(
                        '[login] Ошибка формирования токена. %s. %s',
                        $e->getMessage(),
                        $e->getTraceAsString()
                    )
                );
                $response->setSuccess(false)->addErrors('Ошибка формирования токена.')->setStatus(401);
            }
            return response($response->getResponse(), $response->getStatus());
        }
        return response(
            $response->setSuccess(false)
                ->addErrors('Ошибка авторизации')
                ->setStatus(500)
                ->getResponse(),
            $response->getStatus()
        );
    }

    /**
     * Получение данных о пользователе
     * @return Redirector
     * @throws Exception
     */
    public function index()
    {
        $socialUser = Socialite::driver('vkontakte')->user();
        $email = $socialUser->getEmail();
        $password = Hash::make(random_bytes(32));
        /* @var User $user */
        $user = User::where('email', $email)->first();
        if (!$user) {
            $userData = ['email' => $email, 'password' => $password];
            $user = User::create($userData);
            event(new Registered($user));
        }
        $code = StringHelper::getRandomString(250);
        $user->setAuthCode($code);
        return redirect('/auth/login?social_code=' . $code . '&id=' . $user->id);
    }
}
