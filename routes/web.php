<?php

use Illuminate\Support\Facades\Route;
use App\Modules\main_module\Controllers\MainController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{any}', function ($any = null) {
    preg_match("~^(auth/.*)$~", $any, $matches);
    if (!empty($matches[1])) {
        return view('auth');
    }
    return view('index');
})->where('any', '.*');


//Route::get('/', [\App\Modules\main_module\Controllers\MainController::class, 'index']);

//\Illuminate\Support\Facades\Auth::routes();
//Route::get('/test', [\App\Modules\Test\Controllers\TestController::class, 'index'])->name('home');
//Route::resource('/main', [App\Http\Controllers\MainController::class])->name('main');
