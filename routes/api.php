<?php

use Illuminate\Support\Facades\Route;
use App\Modules\main_module\Controllers\MainController;
use App\Modules\auth\Controllers\AuthController;
use App\Modules\user_interface\Controllers\ProfileController;
use App\Modules\auth\Controllers\VerificationController;
use Laravel\Socialite\Facades\Socialite;
use App\Modules\auth\Controllers\SocialController;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('api/app')->group(function () {
    Route::post('getCids', '\App\Http\Controllers\InfoController@getCids')->name('getCids');
});

Route::prefix('api/auth')->group(function () {

    Route::post('/login/social', [SocialController::class, 'login']);
    Route::post('login', [AuthController::class, 'login'])->middleware('guest')->name('login');
    Route::post('registration', [AuthController::class, 'registration'])
        ->middleware('guest')
        ->name('registration');
    Route::get('user', [AuthController::class, 'user'])
        ->middleware('auth:api')
        ->name('getAuthUser');
    Route::post('logout', [AuthController::class, 'logout'])->middleware('auth:api')->name('logout');

    Route::get('email/verify', [VerificationController::class, 'show'])->name('verification.notice');
    Route::get('email/verify/{id}/{hash}', [VerificationController::class, 'verify'])
        ->name('verification.verify');
    Route::post('email/resend', [VerificationController::class, 'resend'])->name('verification.resend');

    Route::get('/social/redirect', function () {
        return Socialite::driver('vkontakte')->redirect();
    });

    Route::get('/social/callback', [SocialController::class, 'index']);
});

Route::prefix('api')->group(function () {
    Route::get('main', MainController::class . '@index')->name('main');
    Route::get('profile/{id}', ProfileController::class . '@show')
        ->name('getProfile')
        ->middleware('auth:api');
});



