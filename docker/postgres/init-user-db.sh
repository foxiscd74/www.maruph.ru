#!/bin/bash

set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER admin with password 'admin';
    CREATE DATABASE postgres;
    GRANT ALL PRIVILEGES ON DATABASE postgres TO admin;
    ALTER DATABASE postgres SET TIMEZONE TO 'Europe/Moscow';
EOSQL
