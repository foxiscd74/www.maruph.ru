import Vue from "vue";
import Vuex from "vuex";
import cids from './store/modules/cids/data/Cids';
import View from "./store/modules/view/data/View";
import User from "./store/modules/user_interface/data/User";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        cids,
        View,
        User
    }
});
