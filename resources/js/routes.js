import VueRouter from 'vue-router';
import Vue from "vue";
import Main from "./components/content/main/Main";
import Portfolio from "./components/content/main/Portfolio"
import Login from "./components/auth/Login";
import Registration from "./components/auth/Registration";
import Account from "./components/account/Account";

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'main',
        component: Main
    }, {
        path: '/portfolio',
        name: 'portfolio',
        component: Portfolio
    }, {
        path: '/auth/login',
        name: 'login',
        component: Login
    },{
        path: '/auth/registration',
        name: 'registration',
        component: Registration
    },{
        path: '/account/:id',
        name: 'account',
        component: Account
    },
];


export default new VueRouter({
    mode: "history",
    routes,
});
