import axios from "axios";

export default {
    actions: {
        changeTitleStore(ctx, title) {
            ctx.commit('setTittle', title);
        },
        changeLoadingStore(ctx, loading) {
            ctx.commit('setLoading', loading);
        }
    },
    mutations: {
        setTittle(state, title) {
            state.title = title;
        },
        setLoading(state, loading){
            state.loading = loading;
        }
    },
    state: {
        title: '',
        loading: false,
    },
    getters: {
        getTittle(state) {
            return state.title;
        },
        getLoading(state) {
            return state.loading;
        }
    }
}
