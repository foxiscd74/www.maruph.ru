export class ProfileClass {
    id;
    first_name;
    middle_name;
    last_name;
    address;
    created_at;
    updated_at;

    /**
     * Конструктор
     * @param {array}
     */
    constructor(profile) {
        this.id = profile['id'];
        this.first_name = profile['first_name'];
        this.middle_name = profile['middle_name'];
        this.last_name = profile['last_name'];
        this.address = profile['address'];
        this.created_at = profile['created_at'];
        this.updated_at = profile['updated_at'];
    }
}
