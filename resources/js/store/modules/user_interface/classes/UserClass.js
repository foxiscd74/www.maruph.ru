import {ProfileClass} from "./ProfileClass";

export class UserClass {
    id;
    email;
    login;
    profile;
    phone;
    email_verified_at;
    created_at;
    updated_at;

    /**
     * Конструктор
     * @param {user}
     */
    constructor(user, profile = null) {
        this.id = user['id'];
        this.email = user['email'];
        this.login = user['login'];
        this.profile = profile;
        this.phone = user['phone'];
        this.email_verified_at = user['email_verified_at'];
        this.created_at = user['created_at'];
        this.updated_at = user['updated_at'];
    }

    /**
     * Вернуть дочерние сиды
     * @returns {CidClass[]}
     */
    getProfile() {
        if (this.profile) {
            return new ProfileClass(this.profile);
        }
    }
}
