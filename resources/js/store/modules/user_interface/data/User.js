import axios from "axios";
import {eventBus} from "../../../../app";
import {UserClass} from "../classes/UserClass";
import {isEmpty} from "lodash";

export default {
    actions: {
        /**
         * Получить профиль пользователя
         * @returns {Promise<void>}
         */
        async getProfile(ctx, user_id) {
            if (user_id && isEmpty(this.state.User.users[user_id])) {
                await axios.get('/api/profile/' + user_id)
                    .then(res => {
                        if (res.data.user) {
                            const user = res.data.user;
                            const profile = res.data.profile;
                            ctx.commit('updateUser', {user, profile});
                        } else {
                            ctx.commit('updateUser', {user: null});
                        }
                    })
                    .catch(error => {
                        // eventBus.$emit('errorMessages', error.response.data.message);
                    });
            } else {
                ctx.commit('updateUser', {
                    user: this.state.User.users[user_id],
                    profile: this.state.User.users[user_id].profile
                });
            }
        },
        /**
         * Получение авторизованного пользователя
         * @param ctx
         * @returns {Promise<void>}
         */
        async getAuthUser(ctx) {
            await axios.get('/api/auth/user')
                .then(res => {
                    console.log(res.data);
                    if (res.data.user) {
                        const user = res.data.user;
                        const profile = res.data.profile;
                        ctx.commit('updateAuthUser', {user, profile});
                    } else {
                        ctx.commit('updateAuthUser', {user: null});
                    }
                })
                .catch(error => {
                    eventBus.$emit('errorMessages', error.response.data.message);
                });
        }
    },
    mutations: {
        /**
         * Изменить авторизованного пользователя
         * @param state
         * @param user
         * @param profile
         */
        updateAuthUser(state, {user, profile}) {
            state.auth = new UserClass(user, profile);
        },
        /**
         * Изменить пользователя
         * @param state
         * @param user
         * @param profile
         */
        updateUser(state, {user, profile}) {
            state.user = new UserClass(user, profile);
            if (!state.users[user.id]) {
                state.users[user.id] = new UserClass(user, profile);
            }
        },
    },
    state: {
        user: null,
        users: [],
        auth: null,
    },
    getters: {
        /**
         * Получить пользователя
         * @param state
         * @returns {boolean|*}
         */
        getUser(state) {
            if (!state.user) {
                return false;
            }
            return state.user;
        },
        /**
         * Получить авторизованного пользователя
         * @param state
         * @returns {{email}|{}|boolean}
         */
        getAuthUser(state) {
            if (!state.auth) {
                return false;
            }
            return state.auth;
        },
    }
}
