import {CidClass} from "../classes/CidClass";

/**
 * @name CidsService
 * @description Сервис сидов
 * @property cids CidClass[]
 */
export class CidsService {

    cids = [];
    type = self.main_menu_type;
    groupe = self.main_groupe;

    static main_menu_type = 'main_menu';
    static main_groupe = 'main';

    /**
     * @param {CidClass[]} cids
     */
    constructor(cids) {
        var pool_cids = this.cids;
        this._pushCids(cids, pool_cids);
    }

    /**
     * Добавить сиды
     * @param cids
     * @param pool
     * @private
     */
    _pushCids(cids, pool) {
        cids.forEach(function (cid) {
            pool.push(new CidClass(cid));
        });
    }

    /**
     * @param name
     * @returns {CidClass}
     */
    getCidByName(name) {
        return new CidClass(this.searchBy('name', name, this.cids));
    }

    /**
     * @param number
     * @returns {CidClass}
     */
    getCidByNumber(number) {
        const searchData = this.searchBy('number', number, this.cids);
        if (searchData) {
            return new CidClass(searchData);
        }
    }

    /**
     * @param route
     * @returns {CidClass}
     */
    getCidByRoute(route) {
        const searchData = this.searchBy('route', route, this.cids);
        if (searchData) {
            return new CidClass(searchData);
        }
    }

    /**
     * Поиск по значению
     * @param searchBy {string}
     * @param search {mix}
     * @param data {childs}
     * @returns {null|CidClass}
     */
    searchBy(searchBy = 'number', search, data) {
        var obj = this;
        var returnData = null;
        data.forEach(value => {
            if (returnData == null) {
                if (value[searchBy] == search) {
                    returnData = value;
                } else if (value.childs) {
                    returnData = obj.searchBy(searchBy, search, value.childs);
                }
            }
        });
        if (returnData != null) {
            return returnData;
        }
    }
}
