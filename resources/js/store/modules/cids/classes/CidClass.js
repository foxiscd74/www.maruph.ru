export class CidClass {
    name;
    link;
    weight;
    route;
    number;
    childs = [];

    /**
     * Конструктор
     * @param cid
     * @param number
     */
    constructor(cid) {
        this.number = cid['number'];
        this.name = cid['name'];
        this.link = cid['link'];
        this.weight = cid['weight'];
        this.route = cid['route'];
        if (cid['childs']) {
            this.childs = cid['childs'];
        }
    }

    /**
     * Вернуть дочерние сиды
     * @returns {CidClass[]}
     */
    getChilds() {
        var childs = [];
        this.childs.forEach(child => {
            childs.push(new CidClass(child));
        })
        return childs;
    }
}
