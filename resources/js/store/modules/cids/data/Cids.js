import axios from "axios";
import {CidsService} from "../services/CidsService";

export default {
    actions: {
        /**
         *
         * Получить сиды
         * @param ctx
         * @param {type,groupe}
         * @returns {Promise<void>}
         */
        async getCIDS(ctx, {type = null}) {
            await axios.post('/api/app/getCids')
                .then(res => {
                    const allCids = res.data;
                    if (type == 'main_menu') {
                        const cids_main = new CidsService(allCids[type]);
                        ctx.commit('updateCidsMain', cids_main);
                    }
                    ctx.commit('updateAllCids', allCids);
                });
        }
    },
    mutations: {
        /**
         * Изменения сидов
         * @param state
         * @param {CidsService} cids
         */
        updateCidsMain(state, cids) {
            state.cids_main = cids;
        },
        /**
         * Изменения списка всех сидов
         * @param state
         * @param {array} allCids
         */
        updateAllCids(state, allCids) {
            state.allCids = allCids;
        }
    },
    state: {
        cids_main: {},
        allCids: {},
    },
    getters: {
        /**
         * @param state
         * @returns CidsService
         */
        getCids(state) {
            return state.cids_main;
        },
        /**
         * @param state
         * @returns array
         */
        getAllCids(state) {
            return state.allCids;
        }
    }
}
