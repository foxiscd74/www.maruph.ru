/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('v-header', require('./components/Header.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//styles
// import './../imgs/backgrounds/header.jpg';
import router from "./routes";
import vHeader from "./components/main_components/Header";
import vFooter from "./components/main_components/Footer";
import vErrors from "./components/main_components/Errors";
import store from './store';
import {ViewHelper} from "./src/helpers/view/ViewHelper";

export const eventBus = new Vue();

const app = new Vue({
    el: '#app',
    store,
    router,
    mixins: [ViewHelper],
    components: {
        vHeader,
        vFooter,
        vErrors
    },
    data() {
        return {
            fileAsset: '/public/assets/'
        }
    },
    mounted() {
        this.run_action();
        eventBus.$on('set-link', data => {
            this.setLink(data);
        });
        eventBus.$on('runAuth', () => {
            this.runAuth();
        });
        eventBus.$on('errorMessages', messages => {
        });
    },
});
