import {CidsService} from "../../../store/modules/cids/services/CidsService";
import {mapActions, mapGetters} from "vuex";

export const ViewHelper = {
    data() {
        return {
            loading: false,
            link: {},
            token: null,
            errorMessages: []
        }
    },
    methods: {

        ...mapActions(['getCIDS', 'changeTitleStore', 'changeLoadingStore', 'getProfile','getAuthUser']),

        setLink(data, searchBy = 'number') {
            if (searchBy == "number") {
                if (this.$store.getters.getCids.getCidByNumber(data)) {
                    this.link = this.$store.getters.getCids.getCidByNumber(data);
                }
            } else if (searchBy == 'route') {
                this.link = this.$store.getters.getCids.getCidByRoute(data)
                    ? this.$store.getters.getCids.getCidByRoute(data)
                    : {};
            }
            this.changeTitle();
        },

        changeTitle(newTitle = null) {
            if (newTitle !== null) {
                this.changeTitleStore(this.link.name);
                document.title = this.link.name;
            } else if (Object.keys(this.link).length > 0) {
                this.changeTitleStore(this.link.name);
                document.title = this.link.name;
            }
        },

        async run_action() {
            this.runAuth();
            await this.getCIDS({type: CidsService.main_menu_type});
            this.setLink(this.$route.name, 'route');
            setTimeout(() => {
                this.changeLoadingStore(true)
            }, 700);
        },

        runAuth() {
            var token = localStorage.getItem('access_token');
            if (token) {
                axios.defaults.headers.common['Authorization'] = token;
                this.getAuthUser();
            } else {
                axios.defaults.headers.common['Authorization'] = null;
            }
        },

        getTitle() {
            return this.title;
        }
    },
    computed: mapGetters(['getLoading', 'getCids'])
}
