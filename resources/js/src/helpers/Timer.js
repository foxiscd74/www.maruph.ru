export const Timer = {
    data() {
        return {
            currentTime: 0,
            timer: null,
        }
    },
    methods: {
        startTimer(currentTime = 25) {
            this.currentTime = currentTime;
            this.timer = setInterval(() => {
                this.currentTime--
            }, 1000)
        },
        stopTimer() {
            clearTimeout(this.timer)
        },
    },
    watch: {
        currentTime(time) {
            if (time === 0) {
                this.stopTimer()
            }
        }
    }
}
