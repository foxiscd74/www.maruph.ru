<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>MaruPh</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;1,400&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/uikit.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/uikit-rtl.css') }}">
    <link rel="stylesheet" href="{{ asset('css/greg_app.css') }}">
</head>
<body class="antialiased">

<div id="app">
    <div v-if="getLoading">
        <v-header :links="getCids.cids"></v-header>
        <div class="fox_main_content">
            <v-errors :errors="errorMessages"></v-errors>
            <router-view>
            </router-view>
        </div>
        <v-footer></v-footer>
    </div>
    <div v-else>
        <div id="page-preloader">
            <span class="spinner"></span>
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/uikit.js') }}"></script>
<script src="{{ asset('assets/js/uikit-icons.js') }}"></script>
<script src="{{asset('/js/app.js')}}"></script>
</body>
</html>
